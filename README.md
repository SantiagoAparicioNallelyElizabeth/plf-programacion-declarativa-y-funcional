# Programación declarativa y funcional
## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

Videos de referencia

[Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)

[Programación Declarativa. Orientaciones y pautas para el estudio](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)

[Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)


```plantuml
@startmindmap
*[#48005E] <font color=#FFFFFF>Programación Declarativa. \n<font color=#FFFFFF>Orientaciones y pautas \n<font color=#FFFFFF>para el estudio
	
	*_ es
		*[#732487] <font color=#FFFFFF>Un estilo
			*_ conocido como
				*[#C59FCB] Un paradigma.
			*_ surge como
				*[#C59FCB] Reacción a la programación clásica.
		*[#732487] <font color=#FFFFFF>Programación perezosa
			*_ consiste en
				*[#C59FCB] Las tareas rutinarias de programación
					*_ se 
						*[#FFFFFF] Dejan al compilador.
		*[#732487] <font color=#FFFFFF>Programación burocrática
			*_ se encarga de
				*[#C59FCB] La gestión 
					*_ detallada 
						*[#FFFFFF] De la memoria del ordenador.
			*_ sigue
				*[#C59FCB] Secuencias de órdenes.
		*[#732487] <font color=#FFFFFF>Programación creativa
			*_ busca
				*[#C59FCB] Formas de resolver un problema.
				*[#C59FCB] Ahorrar memoria.
			*_ se
				*[#C59FCB] Basa en ideas.

	*_ el
		*[#732487] <font color=#FFFFFF>Programador
			*_ da
				*[#C59FCB] Demasiados detalles sobre los cálculos
					*_ es
						*[#FFFFFF] Tedioso.
						*[#FFFFFF] Toma más tiempo.
			*_ el
				*[#C59FCB] Instrumento esencial  
					*_ es la 
						*[#FFFFFF] Asignación mediante la cual se modifica el estado de la memoria del ordenador pasos a paso.
					*_ presenta
						*[#FFFFFF] Cuello de botella intelectual.

	*_ tiene la
		*[#732487] <font color=#FFFFFF>Idea
			*_ de
				*[#C59FCB] Liberarse de las asignaciones.
				*[#C59FCB] Liberarse de detallar 
					*_ el 
						*[#FFFFFF] Control de la gestión de memoria del ordenador.
				*[#C59FCB] Utilizar otros recursos 
					*_ permitiendo 
						*[#FFFFFF] Especificar programas a un nivel más alto.

	*_ tiene
		*[#732487] <font color=#FFFFFF> Proposito
			*_ el cual es
				*[#C59FCB] Reducir 
					*_ la 
						*[#FFFFFF] Complejidad del programa.
					*_ el 
						*[#FFFFFF] Riesgo de cometer errores.
		*[#732487] <font color=#FFFFFF>Ventajas
			*_ como
				*[#C59FCB] Programas más cortos
					*_ obteniendo
						*[#FFFFFF] Menor tiempo de desarrollo de una aplicación.
				*[#C59FCB] Fáciles de realizar y depurar
					*_ logrando
						*[#FFFFFF] Menor tiempo de modificación y depuración.
		*[#732487] <font color=#FFFFFF>Desventajas
			*_ requiere
				*[#C59FCB] Ideas abstractas.
				*[#C59FCB] Orden.

	*_ considera
		*[#732487] <font color=#FFFFFF> Programación imperativa
			*_ se basa en
				*[#C59FCB] El modelo de Von Neumann
					*_ es decir
						*[#FFFFFF] Gestión detallada de la memoria.

	*_ sus
		*[#732487] <font color=#FFFFFF> Variantes
			*_ son
				*[#C59FCB] Programación funcional
					*_ se basa en
						*[#FFFFFF] Modelo en el que los matemáticos definen las funciones.
							*_ mediante
								*[#FFFFFF] Reglas de escritura.
								*[#FFFFFF] Simplificación de expresiones.
					*_ tiene
						*[#FFFFFF] Características
							*_ como
								*[#FFFFFF] Asistencia de funciones de orden superior.
									*_ es decir
										*[#FFFFFF] Aplican programas a otros programas en una sucesión infinita.
								*[#FFFFFF] Evaluación perezosa
									*_ consiste en
										*[#FFFFFF] Los cálculos no se realizan hasta que otro posterior los necesite.
									*_ tiene
										*[#FFFFFF] Tipos de datos
											*[#FFFFFF] Infinitos
												*_ por ejemplo
													*[#FFFFFF] Serie de números de Fibonacci.
						*[#FFFFFF] Datos
							*_ de
								*[#FFFFFF] Entrada
									*_ es el
										*[#FFFFFF] Argumento.
								*[#FFFFFF] Salida
									*_ es el
										*[#FFFFFF] Resultado de la función.
				*[#C59FCB] Programación funcional
					*_ se basa en
						*[#FFFFFF] Modelo de la demostración de la lógica y de demostración automática.
							*_ utiliza
								*[#FFFFFF] Axiomas.
								*[#FFFFFF] Reglas de inferencia.
					*_ acude a
						*[#FFFFFF] Lógica de predicados de primer orden
							*_ los cuales
								*[#FFFFFF] No establecen orden entre datos de entrada y salida.
									*_ por lo que
										*[#FFFFFF] Permiten ser más declarativos
					*_ el
						*[#FFFFFF] Intérprete de resolución
							*_ dado
								*[#FFFFFF] Conjunto de relaciones y predicados
									*_ opera
										*[#FFFFFF] Mediante algoritmos.
										*[#FFFFFF] Demostrando predicados
											*_ es decir
												*[#FFFFFF] Procesos inversos.

	*_ la
		*[#732487] <font color=#FFFFFF> Programación orientada a la <font color=#FFFFFF>inteligencia artificial
			*_ se basa en
				*[#C59FCB] Programación declarativa
					*_ utiliza el
						*[#FFFFFF] PROLOG
							*_ considerado
								*[#FFFFFF] Lenguaje estándar
							*_ implementa
								*[#FFFFFF] El concepto de programación lógica
						*[#FFFFFF] Lenguaje List
							*_ tiene
								*[#FFFFFF] Relación con la programación funcional.
							*_ utiliza
								*[#FFFFFF] Modelo de programación imperativa
									*_ es decir
										*[#FFFFFF] Lenguaje híbrido
				*[#C59FCB] Programación funcional pura
					*_ utiliza el
						*[#FFFFFF] Lenguaje haskell
							*_ considerado
								*[#FFFFFF] El lenguaje estándar.
					*_ actúa 
						*[#FFFFFF] Sobre tareas
							*_ de
								*[#FFFFFF] Entrada.
								*[#FFFFFF] Salida.
@endmindmap
```

## Lenguaje de Programación Funcional (2015)

Video de referencia

[Lenguaje de Programación Funcional](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)

```plantuml
@startmindmap
*[#48005E] <font color=#FFFFFF>Programación Funcional
	
	*_ tiene
		*[#732487] <font color=#FFFFFF>Paradigmas de programación
			*_ son
				*[#C59FCB] Modelos de computación 
					*_ en donde
						*[#FFFFFF] Los diferentes lenguajes dotan de semántica los programas.
			*_ el
				*[#C59FCB] Modelo de Von Neumann
					*_ se 
						*[#FFFFFF] Utilizaba en un inicio.

	*_ posee
		*[#732487] <font color=#FFFFFF>Variantes
			*_ como
				*[#C59FCB] Programación orientada a objetos
					*_ son
						*[#FFFFFF] Pequeños trozos de código.
							*_ en donde
								*[#FFFFFF] Los objetos que interactúan entre sí.
					*_ se
						*[#FFFFFF] Compone de instrucciones
							*_ que 
								*[#FFFFFF] Se ejecutan secuencialmente.
				*[#C59FCB] Lógica simbólica
					*_ son un
						*[#FFFFFF] Conjunto de sentencias
							*_ definen
								*[#FFFFFF] Lo que es verdad.
								*[#FFFFFF] Lo que es conocido.
							*_ utiliza
								*[#FFFFFF] Inferencia lógica
									*_ para
										*[#FFFFFF] Resolver el problema.
					*_ su 
						*[#FFFFFF] Funcionamiento
							*_ consiste en
								*[#FFFFFF] Recibir datos.
								*[#FFFFFF] Devuelve definiciones.

	*_ los
		*[#732487] <font color=#FFFFFF>Lenguajes de programación
			*_ que siguen
				*[#C59FCB] El modelo imperativo
					*_ se
						*[#FFFFFF] Les deben de dar órdenes.
							*_ deben ser
								*[#FFFFFF] Secuenciales.
					*_ tiene
						*[#FFFFFF] Instrucción de asignación.
					*_ posee
						*[#FFFFFF] Inconvenientes
							*_ como
								*[#FFFFFF] Presenta problemas colaterales.
								*[#FFFFFF] Mayor riego de cometer errores.
						*[#FFFFFF] Beneficios
							*[#FFFFFF] Facilmente legible.

	*_ el
		*[#732487] <font color=#FFFFFF>Lambda cálculo
			*_ que siguen
				*[#C59FCB] El modelo imperativo
					*_ se
						*[#FFFFFF] Desarrollo
							*_ por
								*[#FFFFFF] Alonzo Church
					*_ inicialmente era
						*[#FFFFFF] Un sistema
							*_ para
								*[#FFFFFF] Estudiar
									*_ las
										*[#FFFFFF] Funciones.
										*[#FFFFFF] Aplicaciones de funciones.
									*_ la
										*[#FFFFFF] Recursividad.
					*_ prosiguió
						*[#FFFFFF] Hasky 
							*_ desarrolla la
								*[#FFFFFF] Lógica combinatoria
									*_ permite
										*[#FFFFFF] Modelar un lenguaje de programación.

	*_ las
		*[#732487] <font color=#FFFFFF>Bases
			*_ el
				*[#C59FCB] Programa
					*_ es una
						*[#FFFFFF] Función matemática
			*_ la 
				*[#C59FCB] Variable
					*_ se utiliza
						*[#FFFFFF] Para referirse
							*_ a los
								*[#FFFFFF] Parámetros
									*_ de
										*[#FFFFFF] Entrada de las funciones.
					*_ pierden
						*[#FFFFFF] El concepto 
							*_ de 
								*[#FFFFFF] Zona modificable de memoria.
							*_ el uso de
								*[#FFFFFF] Bucles desaparece
									*_ se sustituye
										*[#FFFFFF] Recursividad
											*_ es
												*[#FFFFFF] La función que hace referencia así misma.
					*_ solo
						*[#FFFFFF] Se utilizan
							*_ para
								*[#FFFFFF] Almacenamiento temporal.

	*_ sus
		*[#732487] <font color=#FFFFFF>Ventajas 
			*_ son
				*[#C59FCB] Los programas solo dependen
					*_ del
						*[#FFFFFF] parámetro de entrada.
							*_ se conoce como
								*[#FFFFFF] Transparencia referencial.
				*[#C59FCB] Permite escribir código más rápido.
				*[#C59FCB] Los resultados
					*_ son
						*[#FFFFFF] Independientes del orden del cálculo.
				*[#C59FCB] Es posible paralizar las funciones.
				*[#C59FCB] Las funciones pueden estar en todos lados
					*_ se le conoce
						*[#FFFFFF] Ciudadanos de primera clase.

	*_ presenta
		*[#732487] <font color=#FFFFFF>Características 
			*_ las
				*[#C59FCB] Funciones
					*_ pueden
						*[#FFFFFF] Ser parámetros de otras funciones.
					*_ que reciben
						*[#FFFFFF] Otra función como parámetro de entrada 
							*_ se denomina 
								*[#FFFFFF] Función de orden superior.
					*_ la
						*[#FFFFFF] salida de una función 
							*_ es 
								*[#FFFFFF] Otra función.
					*_ No 
						*[#FFFFFF] Depende de otra
							*_ en el
								*[#FFFFFF] Entorno multiprocesador
			*_ la
				*[#C59FCB] currificación
					*_ es
						*[#FFFFFF] Una función
							*_ tiene 
								*[#FFFFFF] Varios parámetros de entrada 
									*_ devuelve 
										*[#FFFFFF] Diferentes funciones de salida.
					*_ se conoce como
						*[#FFFFFF] Aplicación parcial.
			*_ contiene
				*[#C59FCB] La memoizacion
					*_ en donde
						*[#FFFFFF] Solo se evalúa una única vez.
					*_ almacena 
						*[#FFFFFF] El valor de la expresión ya haya sido evaluada.
					*_ se conoce como
						*[#FFFFFF] Pasó por necesidad. 
			*_ tipos
				*[#C59FCB] Evaluaciones
					*_ la
						*[#FFFFFF] Evaluación perezosa
							*_ funcionamiento
								*[#FFFFFF] Evalúa de dentro hacia fuera.
							*_ solo
								*[#FFFFFF] Realiza solo los cálculos necesarios.
					*_ en
						*[#FFFFFF] Lenguajes imperativos
							*_ se tiene
								*[#FFFFFF] Evaluación estrictas.
								*[#FFFFFF] Evaluación impaciente.
							*_ funcionamiento
								*[#FFFFFF] Evalúa de fuera hacia dentro.
@endmindmap
```






















































